let days = 5;

// case 1
console.log(days * 86400);

// case 2
console.log("How many seconds in " + days + " day(-s)? - " + (days * 86400));